#include "Mesh.hpp"
#include "../utils/Perlin.cpp"

#include <iostream>
#include <vector>
#include <exception>
#include <map>
#include <functional>
#include <numeric>

typedef std::vector<std::vector<std::vector<std::pair<int, glm::vec3>>>> norm_data;

MeshPtr MakeSurface(float size, unsigned int frequency, int num_octaves, float persistence) {
    std::vector<glm::vec3> vertices;
    norm_data norm_ij(frequency + 2,
                      std::vector<std::vector<std::pair<int, glm::vec3>>>(frequency + 2,
                                                                          std::vector<std::pair<int, glm::vec3>>()));
    std::vector<glm::vec3> normals;

    std::vector<glm::vec2> tex_coords;
    std::vector<glm::vec2> map_coords;

    Perlin2D pn = Perlin2D();

    // Draw square from -size to size with frequency=frequency
    float step = 2.f / frequency;
    float scale = 5;

    // For norm_ij
    int it = 0;
    int ii = 0;
    int jj = 0;
    for (float i = -1; i < 1; i += step, ++ii) {
        jj = 0;
        for (float j = -1; j < 1; j += step, ++jj) {
            glm::vec3 a = glm::vec3(i * size, j * size, pn.Noise(i, j, num_octaves, persistence));
            glm::vec3 b = glm::vec3((i + step) * size, j * size, pn.Noise(i + step, j, num_octaves, persistence));
            glm::vec3 c = glm::vec3(i * size, (j + step) * size, pn.Noise(i, j + step, num_octaves, persistence));
            vertices.push_back(a);
            vertices.push_back(b);
            vertices.push_back(c);

            glm::vec3 norm = glm::normalize(glm::cross(c - a, b - a));
            norm_ij[ii][jj].emplace_back(it++, -norm);
            norm_ij[ii+1][jj].emplace_back(it++, -norm);
            norm_ij[ii][jj+1].emplace_back(it++, -norm);

            float tx = (i + 1.f) * scale;
            float ty = (j + 1.f) * scale;
            tex_coords.emplace_back(tx, ty);
            tex_coords.emplace_back(tx + step * scale, ty);
            tex_coords.emplace_back(tx, ty + step * scale);

            float x = (i + 1.f) / 2.f;
            float y = (j + 1.f) / 2.f;
            map_coords.emplace_back(x, y);
            map_coords.emplace_back(x + step / 2.f, y);
            map_coords.emplace_back(x, y + step / 2.f);

            a = glm::vec3(i * size, (j + step) * size, pn.Noise(i, j + step, num_octaves, persistence));
            b = glm::vec3((i + step) * size, (j + step) * size, pn.Noise(i + step, j + step, num_octaves, persistence));
            c = glm::vec3((i + step) * size, j * size, pn.Noise(i + step, j, num_octaves, persistence));
            vertices.push_back(a);
            vertices.push_back(b);
            vertices.push_back(c);

            norm = glm::normalize(glm::cross(c - a, b - a));
            norm_ij[ii][jj + 1].emplace_back(it++, norm);
            norm_ij[ii + 1][jj + 1].emplace_back(it++, norm);
            norm_ij[ii + 1][jj].emplace_back(it++, norm);

            tex_coords.emplace_back(tx, ty + step * scale);
            tex_coords.emplace_back(tx + step * scale, ty + step * scale);
            tex_coords.emplace_back(tx + step * scale, ty);
            map_coords.emplace_back(x, y + step / 2.f);
            map_coords.emplace_back(x + step / 2.f, y + step / 2.f);
            map_coords.emplace_back(x + step / 2.f, y);
        }
    }

    normals = std::vector<glm::vec3>(static_cast<unsigned long>(it));
    for (ii = 0; ii < frequency + 1; ++ii) {
        for (jj = 0; jj < frequency + 1; ++jj) {
            glm::vec3 norm;
            for (auto& elem : norm_ij[ii][jj]) {
                norm += elem.second;
            }
            norm /= norm_ij[ii][jj].size();
            for (auto& elem : norm_ij[ii][jj]) {
                normals[elem.first] = norm;
            }
        }
    }

    std::shared_ptr<DataBuffer> buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    std::shared_ptr<DataBuffer> buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    std::shared_ptr<DataBuffer> buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(tex_coords.size() * sizeof(float) * 2, tex_coords.data());

    std::shared_ptr<DataBuffer> buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf3->setData(map_coords.size() * sizeof(float) * 2, map_coords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, buf3);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(static_cast<GLuint>(vertices.size()));

    return mesh;
}