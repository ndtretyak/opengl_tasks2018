#include "Camera.hpp"

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>


void FreeCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void FreeCameraMover::update(GLFWwindow* window, double dt)
{
    float speed = 1.0f;

    glm::vec3 forward_dir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

    glm::vec3 right_dir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS or glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
        _pos += forward_dir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS or glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
        _pos -= forward_dir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS or glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
        _pos -= right_dir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS or glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
        _pos += right_dir * speed * static_cast<float>(dt);
    }

    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    _camera.projMatrix = glm::perspective(glm::radians(45.0f),
                                                      (float) width / height, 0.1f, 100.f);
}

FreeCameraMover::FreeCameraMover() : CameraMover(), _pos(5.0f, 0.0f, 2.5f)
{
    _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f)));
}

void FreeCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{

}

void FreeCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    double dx = xpos - _oldXPos;
    double dy = ypos - _oldYPos;

    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
    _rot *= glm::angleAxis(static_cast<float>(dy * 0.002), rightDir);

    glm::vec3 upDir(0.0f, 0.0f, 1.0f);
    _rot *= glm::angleAxis(static_cast<float>(dx * 0.002), upDir);

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void FreeCameraMover::initPosition(double x, double y) {
    _oldXPos = x;
    _oldYPos = y;
}