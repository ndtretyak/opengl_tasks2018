#include <cmath>
#include <cstdlib>
#include <iostream>

class Perlin2D
{
    int * permutationTable;

public:
    Perlin2D(int seed = 0)
    {
        srand(seed);
        permutationTable = new int[1024];
        for (int i = 0; i < 1024; i++) {
            permutationTable[i] = rand();
        }
    }

    float * GetPseudoRandomGradientVector(int x, int y) {
        int v = (int)(((x * 1836311903) ^ (y * 2971215073) + 4807526976) & 1023);
        v = permutationTable[v] & 3;
        static float result[2];
        switch (v)
        {
            case 0:  result[0] = 1, result[1] = 0; break;
            case 1:  result[0] = -1, result[1] = 0; break;
            case 2:  result[0] = 0, result[1] = 1; break;
            default: result[0] = 0, result[1] = -1;
        }
        return result;
    }

    float QunticCurve(float t) {
        return t * t * t * (t * (t * 6 - 15) + 10);
    }

    float Lerp(float a, float b, float t)
    {
        return a + (b - a) * t;
    }

    float Dot(float a[], float b[])
    {
        return a[0] * b[0] + a[1] * b[1];
    }

    float Noise(float fx, float fy)
    {
        int left = floorf(fx);
        int top = floorf(fy);
        float pointInQuadX = fx - left;
        float pointInQuadY = fy - top;

        float * topLeftGradient = GetPseudoRandomGradientVector(left, top);
        float * topRightGradient = GetPseudoRandomGradientVector(left + 1, top);
        float * bottomLeftGradient = GetPseudoRandomGradientVector(left, top + 1);
        float * bottomRightGradient = GetPseudoRandomGradientVector(left + 1, top + 1);

        static float distanceToTopLeft[] = { pointInQuadX,   pointInQuadY };
        static float distanceToTopRight[] = { pointInQuadX - 1, pointInQuadY };
        static float distanceToBottomLeft[] = { pointInQuadX,   pointInQuadY - 1 };
        static float distanceToBottomRight[] = { pointInQuadX - 1, pointInQuadY - 1 };

        float tx1 = Dot(distanceToTopLeft, topLeftGradient);
        float tx2 = Dot(distanceToTopRight, topRightGradient);
        float bx1 = Dot(distanceToBottomLeft, bottomLeftGradient);
        float bx2 = Dot(distanceToBottomRight, bottomRightGradient);

        pointInQuadX = QunticCurve(pointInQuadX);
        pointInQuadY = QunticCurve(pointInQuadY);

        float tx = Lerp(tx1, tx2, pointInQuadX);
        float bx = Lerp(bx1, bx2, pointInQuadX);
        float tb = Lerp(tx, bx, pointInQuadY);

        return tb;
    }

    float Noise(float fx, float fy, int octaves, float persistence = 0.5f)
    {
        float amplitude = 1;
        float max = 0;
        float result = 0;

        while (octaves-- > 0)
        {
            max += amplitude;
            result += Noise(fx, fy) * amplitude;
            amplitude *= persistence;
            fx *= 2;
            fy *= 2;
        }
        return result / max;
    }
};
