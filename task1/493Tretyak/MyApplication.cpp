#include <iostream>

#include "MyApplication.hpp"

MyApplication::MyApplication() {
    _cameraMover = std::make_shared<FreeCameraMover>(); // Handler
}


void MyApplication::makeScene() {
    Application::makeScene();

    _mesh = MakeSurface(2, 5, 2, 2);
    _mesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));
    _shader = std::make_shared<ShaderProgram>("493TretyakData/shader.vert", "493TretyakData/shader.frag");
    _another_shader = std::make_shared<ShaderProgram>(
            "493TretyakData/another_shader.vert", "493TretyakData/shader.frag"
    );
}

void MyApplication::draw() {
    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonOffset(1.0, 10.0);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    _shader->use();
    _shader->setMat4Uniform("view_matrix", _camera.viewMatrix);
    _shader->setMat4Uniform("projection_matrix", _camera.projMatrix);
    _shader->setMat4Uniform("model_matrix", _mesh->modelMatrix());
    _mesh->draw();

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    _another_shader->use();
    _another_shader->setMat4Uniform("view_matrix", _camera.viewMatrix);
    _another_shader->setMat4Uniform("projection_matrix", _camera.projMatrix);
    _another_shader->setMat4Uniform("model_matrix", _mesh->modelMatrix());
    _mesh->draw();
}
