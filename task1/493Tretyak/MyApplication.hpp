#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "utils/Perlin.cpp"

class MyApplication : public Application {
public:
    MyApplication();
    void makeScene() override;
    void draw() override;
protected:
    MeshPtr _mesh;
    std::shared_ptr<ShaderProgram>  _shader;
    std::shared_ptr<ShaderProgram>  _another_shader;
};
