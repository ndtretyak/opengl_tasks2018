/*
Преобразует координаты вершины из локальной системы координат в Clip Space.
Копирует цвет вершины из вершинного атрибута в выходную переменную color.
*/

#version 330

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec4 vertexColor;

out vec4 color;

void main()
{
	color = vec4(0.0, 0.0, 0.0, 0.0);
	color.r = vertexPosition.z + 1;
	color.g = vertexPosition.x * 2;
	color.b = vertexPosition.y * 3;

    gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vertexPosition, 1.0);
}